//import mongoose
const mongoose = require ("mongoose");
//import user model
const userModel = require("../models/userModel");

//function create user
const createUser = (request, response) => {
    //B1: chuẩn bị dữ liệu
    let body = request.body;

    //B2: validate dữ liệu
    if(!body.address.geo.lat){
        return response.status(400).json({
            status:"Bad request",
            message: "Lat is not valid"
        })
    }
    if(!body.address.geo.lng){
        return response.status(400).json({
            status:"Bad request",
            message: "lng is not valid"
        })
    }
    if(!body.address.street){
        return response.status(400).json({
            status:"Bad request",
            message: "Street is not valid"
        })
    }
    if(!body.address.suite){
        return response.status(400).json({
            status:"Bad request",
            message: "Suite is not valid"
        })
    }
    if(!body.address.city){
        return response.status(400).json({
            status:"Bad request",
            message: "City is not valid"
        })
    }
    if(!body.address.zipcode){
        return response.status(400).json({
            status:"Bad request",
            message: "Zipcode is not valid"
        })
    }
    if(!body.company.name){
        return response.status(400).json({
            status:"Bad request",
            message: "Company name is not valid"
        })
    }
    if(!body.company.catchPhrase){
        return response.status(400).json({
            status:"Bad request",
            message: "Catch Phrase is not valid"
        })
    }
    if(!body.company.bs){
        return response.status(400).json({
            status:"Bad request",
            message: "Bs is not valid"
        })
    }
    if(!body.name){
        return response.status(400).json({
            status:"Bad request",
            message: "Name is not valid"
        })
    }
    if(!body.username){
        return response.status(400).json({
            status:"Bad request",
            message: "User Name is not valid"
        })
    }
    if(!body.phone){
        return response.status(400).json({
            status:"Bad request",
            message: "Phone is not valid"
        })
    }
    if(!body.website){
        return response.status(400).json({
            status:"Bad request",
            message: "Website is not valid"
        })
    }
    //B3: thao tác với CSDL
    let newUser =  {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        username: body.username,
        address: {
            street:  body.address.street,
            suite: body.address.suite,
            city: body.address.city,
            zipcode: body.address.zipcode,
            geo: {
                lat: body.address.geo.lat,
                lng: body.address.geo.lng
            }
        },
        phone: body.phone,
        website: body.website,
        company: {
            name: body.company.name,
            catchPhrase: body.company.catchPhrase,
            bs: body.company.bs
        }
    }
    console.log(newUser);
    if(body.address.geo.lat !== undefined){
        newUser.address.geo.lat = body.address.geo.lat
    }
    if(body.address.geo.lng !== undefined){
        newUser.address.geo.lng = body.address.geo.lng
    }
    if(body.address.street !== undefined){
        newUser.address.street = body.address.street
    }
    if(body.address.suite !== undefined){
        newUser.address.suite = body.address.suite
    }
    if(body.address.city !== undefined){
        newUser.address.city = body.address.city
    }
    if(body.address.zipcode !== undefined){
        newUser.address.zipcode = body.address.zipcode
    }
    if(body.company.name !== undefined){
        newUser.company.name = body.company.name
    }
    if(body.company.catchPhrase !== undefined){
        newUser.company.catchPhrase = body.company.catchPhrase
    }
    if(body.company.bs !== undefined){
        newUser.company.bs = body.company.bs
    }
    if(body.name !== undefined){
        newUser.name = body.name
    }
    if(body.username !== undefined){
        newUser.username = body.username
    }
    if(body.phone !== undefined){
        newUser.phone = body.phone
    }
    if(body.website !== undefined){
        newUser.website = body.website
    }
    userModel.create(newUser, (error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new user successfully",
            data: data
        })
    })
}
//function get all user
const getAllUser = (request,response) => {
    userModel.find((error, data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Get all users successfully",
            data: data
        })
    })
}
//function update user by id
const updateUser = (request, response) => {
    //B1: chuẩn bị dữ liệu
    let userId = request.params.userId;
    let body = request.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
            status:"Bad request",
            message:"User id is not valid"
        })
    }
    if(!body.address.geo.lat){
        return response.status(400).json({
            status:"Bad request",
            message: "Lat is not valid"
        })
    }
    if(!body.address.geo.lng){
        return response.status(400).json({
            status:"Bad request",
            message: "lng is not valid"
        })
    }
    if(!body.address.street){
        return response.status(400).json({
            status:"Bad request",
            message: "Street is not valid"
        })
    }
    if(!body.address.suite){
        return response.status(400).json({
            status:"Bad request",
            message: "Suite is not valid"
        })
    }
    if(!body.address.city){
        return response.status(400).json({
            status:"Bad request",
            message: "City is not valid"
        })
    }
    if(!body.address.zipcode){
        return response.status(400).json({
            status:"Bad request",
            message: "Zipcode is not valid"
        })
    }
    if(!body.company.name){
        return response.status(400).json({
            status:"Bad request",
            message: "Company name is not valid"
        })
    }
    if(!body.company.catchPhrase){
        return response.status(400).json({
            status:"Bad request",
            message: "Catch Phrase is not valid"
        })
    }
    if(!body.company.bs){
        return response.status(400).json({
            status:"Bad request",
            message: "Bs is not valid"
        })
    }
    if(!body.name){
        return response.status(400).json({
            status:"Bad request",
            message: "Name is not valid"
        })
    }
    if(!body.username){
        return response.status(400).json({
            status:"Bad request",
            message: "User Name is not valid"
        })
    }
    if(!body.phone){
        return response.status(400).json({
            status:"Bad request",
            message: "Phone is not valid"
        })
    }
    if(!body.website){
        return response.status(400).json({
            status:"Bad request",
            message: "Website is not valid"
        })
    }
    //B3: gọi model chứa dữ liệu 
    let userUpdate = {
        name: body.name,
        username: body.username,
        address: {
            street:  body.address.street,
            suite: body.address.suite,
            city: body.address.city,
            zipcode: body.address.zipcode,
            geo: {
                lat: body.address.geo.lat,
                lng: body.address.geo.lng
            }
        },
        phone: body.phone,
        website: body.website,
        company: {
            name: body.company.name,
            catchPhrase: body.company.catchPhrase,
            bs: body.company.bs
        }
    };
    if(body.address.geo.lat != undefined){
        userUpdate.address.geo.lat = body.address.geo.lat
    }
    if(body.address.geo.lng !==undefined){
        userUpdate.address.geo.lng = body.address.geo.lng
    }
    if(body.address.street !==undefined){
        userUpdate.address.street = body.address.street
    }
    if(body.address.suite !==undefined){
        userUpdate.address.suite = body.address.suite
    }
    if(body.address.city !==undefined){
        userUpdate.address.city = body.address.city
    }
    if(body.address.zipcode !==undefined){
        userUpdate.address.zipcode = body.address.zipcode
    }
    if(body.company.name !==undefined){
        userUpdate.company.name = body.company.name
    }
    if(body.company.catchPhrase !==undefined) {
        userUpdate.company.catchPhrase = body.company.catchPhrase
    }
    if(body.company.bs !==undefined){
        userUpdate.company.bs = body.company.bs
    }
    if(body.name !==undefined){
        userUpdate.name = body.name
    }
    if(body.username !==undefined){
        userUpdate.username = body.username
    }
    if(body.phone !==undefined){
        userUpdate.phone = body.phone
    }
    if(body.website !==undefined){
        userUpdate.website = body.website
    }
    userModel.findByIdAndUpdate(userId,userUpdate,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Update user is successfully",
            updateUser: data
        })
    })
}
//function get user by id
const getUserById = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let userId =  request.params.userId;
    //B2: validate id
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
            status:"Bad request",
            message:"User Id is not valid"
        })
    }
    //B3: gọi model chứa dữ liệu
    userModel.findById(userId,(error,data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get user by id ${userId} successfully`,
            data: data
        })
    })
}
//function delete user by id
const deleteUser = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
            status: "Bad request",
            message:"User id is not valid"
        })
    }
    //B3: gọi model chứa dữ liệu cần xóa
    userModel.findByIdAndDelete(userId,(error, data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Delete user with id ${userId} successfully`,
        })
    })
}
module.exports = {
    createUser,
    getAllUser,
    updateUser,
    getUserById,
    deleteUser
}