//khai báo thư viện mongoose
const mongoose = require("mongoose");
//khai báo schema
const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
    userId: {
        type: mongoose.Types.ObjectId,
        ref: "user",
        require: true
    },
    title: {
        type: String,
        require: true
    }
})
module.exports =mongoose.model("album", AlbumSchema)